# GKE-DEMO
a simple demo for GKE

## What are the core technologies/projects used?
- Use Docker to build the container
- Use GKE to deploy the container
- Use Pulumi to manage the infrastructure

## How to run
1. Install Pulumi
   - mac
   ```bash
   brew install pulumi/tap/pulumi
   ```
   - windows
   ```bash
   curl -fsSL https://get.pulumi.com | sh
   ```
2. Install GCP SDK
   - mac & linux
   ```bash
   curl https://sdk.cloud.google.com | bash
   exec -l $SHELL
   gcloud init
   ```
3. Set the project-id in Pulumi to create gcp resources
   ```bash
   pulumi config set gcp:project <project-id>
   cd ./iac
   pulumi up -y
   ```
4. 
