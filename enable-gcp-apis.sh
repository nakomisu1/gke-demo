#!/bin/bash

echo "請輸入您的GCP專案ID:"
IFS= read -r PROJECT_ID

# Compute Engine API
gcloud services enable compute.googleapis.com --project="${PROJECT_ID}"

# Kubernetes Engine API
gcloud services enable container.googleapis.com --project="${PROJECT_ID}"

# Cloud Storage API
gcloud services enable storage-component.googleapis.com --project="${PROJECT_ID}"

# Identity and Access Management (IAM) API
gcloud services enable iam.googleapis.com --project="${PROJECT_ID}"

# 打印完成消息
echo "All specified APIs have been enabled for project ${PROJECT_ID}."
