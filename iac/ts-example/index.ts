import * as pulumi from "@pulumi/pulumi";
import * as gcp from "@pulumi/gcp";

// Get some provider-namespaced configuration values
const providerCfg = new pulumi.Config("gcp");
const gcpProject = providerCfg.require("project");
const gcpRegion = providerCfg.get("region") || "us-central1";
// Get some other configuration values or use defaults
const cfg = new pulumi.Config();
const nodesPerZone = cfg.getNumber("nodesPerZone") || 1;

// Create a new network
const gkeNetwork = new gcp.compute.Network("gke-network", {
    autoCreateSubnetworks: false,
    description: "A virtual network for your GKE cluster(s)",
});

// Create a new subnet in the network created above
const gkeSubnet = new gcp.compute.Subnetwork("gke-subnet", {
    ipCidrRange: "10.128.0.0/12",
    network: gkeNetwork.id,
    privateIpGoogleAccess: true,
    // logConfig: {
    //     aggregationInterval: "INTERVAL_5_MIN",
    //     flowSampling: 0.5,
    //     metadata: "INCLUDE_ALL_METADATA",
    // },
});

// Create a new GKE cluster
const gkeCluster = new gcp.container.Cluster("gke-cluster", {
    description: "A GKE cluster",
    location: gcpRegion,
    initialNodeCount: 1,                    // 初始節點數 (選填)
    networkingMode: "VPC_NATIVE",          // 網路模式 (選填)
    binaryAuthorization: {
        evaluationMode: "PROJECT_SINGLETON_POLICY_ENFORCE",
    },
    datapathProvider: "ADVANCED_DATAPATH", // 跟 networkPolicy 無法並存
    ipAllocationPolicy: {
        clusterIpv4CidrBlock: "/14",
        servicesIpv4CidrBlock: "/20",
    },

    removeDefaultNodePool: true,
    releaseChannel: {
        channel: "STABLE",
    },
    workloadIdentityConfig: {
        workloadPool: `${gcpProject}.svc.id.goog`,
    },
    monitoringConfig: {
        managedPrometheus: {
            enabled: true,                       // 啟用 Google Managed Prometheus (選填)
        },
    },
    addonsConfig: {
        dnsCacheConfig: {
            enabled: true,
        },
    },
    resourceLabels: {
        "project": "gke-demo",
        "env": "dev",
    },

    // Pod 安全性政策
    podSecurityPolicyConfig: {
        enabled: true,                        // 啟用 Pod 安全性政策 (選填)
    },

    network: gkeNetwork.name,
    subnetwork: gkeSubnet.name,
    masterAuthorizedNetworksConfig: {
        cidrBlocks: [{
            cidrBlock: "10.128.0.0/12",
            displayName: "allow gke-subnet",
        }],
    },
    privateClusterConfig: {
        enablePrivateNodes: true,
        enablePrivateEndpoint: false,
        masterIpv4CidrBlock: "10.100.0.0/28", // 避免與其他 subnet ip 範圍相同
    },
});

// Create a service account for the node pool
const gkeFrontendNodepoolSa = new gcp.serviceaccount.Account("gke-frontend-nodepool-sa", {
    accountId: pulumi.interpolate `${gkeCluster.name}-fnp-1-sa`,
    displayName: "Frontend Nodepool 1 Service Account",
});

const gkeBackendNodepoolSa = new gcp.serviceaccount.Account("gke-backend-nodepool-sa", {
    accountId: pulumi.interpolate `${gkeCluster.name}-bnp-1-sa`,
    displayName: "Backend Nodepool 1 Service Account",
});

// Create a nodepool for the GKE cluster
const gkeFrontendNodepool = new gcp.container.NodePool("gke-frontend-nodepool-1", {
    cluster: gkeCluster.id,
    initialNodeCount: 1, // 初始節點數
    nodeConfig: {
        machineType: "e2-medium", // 選擇您的機器類型
        diskSizeGb: 50, // 磁碟大小
        diskType: "pd-standard", // 磁碟類型
        labels: {
            "app":  "frontend",
            "disk_type": "standard",
        },
        tags: [
            "dev-gke",
            "backend"
        ],
        oauthScopes: [
            "https://www.googleapis.com/auth/cloud-platform"
        ],
        serviceAccount: gkeFrontendNodepoolSa.email,

    },
    management: {
        autoRepair: true, // 啟用節點自動修復
        autoUpgrade: true, // 啟用節點自動升級
    },
    autoscaling: {
        maxNodeCount: 10, // 最大節點數
        minNodeCount: 1, // 最小節點數
    },
    upgradeSettings: {
        maxSurge: 1, // 最大擴展
        maxUnavailable: 1, // 最大不可用
    },
});

const gkeBackendNodepool = new gcp.container.NodePool("gke-backend-nodepool-1", {
    cluster: gkeCluster.id,
    initialNodeCount: 1, // 初始節點數
    nodeConfig: {
        machineType: "e2-medium", // 選擇您的機器類型
        diskSizeGb: 50, // 磁碟大小
        diskType: "pd-standard", // 磁碟類型
        labels: {
            "app":  "backend",
            "disk_type": "standard",
        },
        tags: [
            "dev-gke",
            "backend"
        ],
        oauthScopes: [
            "https://www.googleapis.com/auth/cloud-platform"
        ],
        serviceAccount: gkeBackendNodepoolSa.email,

    },
    management: {
        autoRepair: true, // 啟用節點自動修復
        autoUpgrade: true, // 啟用節點自動升級
    },
    autoscaling: {
        maxNodeCount: 10, // 最大節點數
        minNodeCount: 1, // 最小節點數
    },
    upgradeSettings: {
        maxSurge: 1, // 最大擴展
        maxUnavailable: 1, // 最大不可用
    },
});

// Build a Kubeconfig for accessing the cluster
const clusterKubeconfig = pulumi.interpolate `apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: ${gkeCluster.masterAuth.clusterCaCertificate}
    server: https://${gkeCluster.endpoint}
  name: ${gkeCluster.name}
contexts:
- context:
    cluster: ${gkeCluster.name}
    user: ${gkeCluster.name}
  name: ${gkeCluster.name}
current-context: ${gkeCluster.name}
kind: Config
preferences: {}
users:
- name: ${gkeCluster.name}
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1beta1
      command: gke-gcloud-auth-plugin
      installHint: Install gke-gcloud-auth-plugin for use with kubectl by following
        https://cloud.google.com/blog/products/containers-kubernetes/kubectl-auth-changes-in-gke
      provideClusterInfo: true
`;

// Export some values for use elsewhere
export const networkName = gkeNetwork.name;
export const networkId = gkeNetwork.id;
export const clusterName = gkeCluster.name;
export const clusterId = gkeCluster.id;
export const kubeconfig = clusterKubeconfig;
