#!/bin/bash

ORI=$(pwd)
cd "$(dirname "$0")" || exit
SCRIPTDIR=$(pwd)

filelist=( state-metris.yaml ingress.yaml grafana.yaml frontend.yaml)
for resource in "${filelist[@]}"; do
  kubectl delete -n gmp-public -f "$resource";
done
